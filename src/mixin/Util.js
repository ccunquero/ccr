
var moment = require("moment");
export default {
   data() {

   },
   methods: {
      messageSucces: function (context, message) {
         context.$q.notify({
            color: 'positive',
            icon: "check",
            message: message,
            position: "top",
         })

      },
      messageError(context, message) {
         context.$q.notify({
            color: 'red',
            icon: "error",
            message: message,
            position: "top",
         })

      },
      formatData(data, hideState) {

         var newData = []
         if (!(data.constructor === Array)) return

         data.map(item => {
            var element = {}
            var subElment = {}
            var subTitle = []

            element.id = item.idTar
            element.title = item.descripcionActividad
            element.company = item.nombreEmp
            element.isNew = this.validIsNew(item.idTar)

            subElment.name = "Fecha entrega"
            subElment.value = this.getFormatDate(item.fechaEntregaTar, false)
            subElment.value2 = this.getFormatDate(item.fechaEntregaTar, true)
            subTitle.push(subElment)
            subElment = {}


            subElment.name = "Entidad a enviar"
            subElment.value = item.descripcionEntEnviar
            subElment.show = hideState
            subTitle.push(subElment)
            subElment = {}

            subElment.name = "Estado"
            subElment.value = item.descripcionEta
            subElment.show = !hideState
            subTitle.push(subElment)
            subElment = {}

            subElment.name = "Usuario"
            subElment.value = item.nombreUsu
            subElment.show = !hideState
            subTitle.push(subElment)
            subElment = {}


            subElment.name = "Motivo rechazo"
            subElment.value = item.motivoRechazo
            subElment.show = !hideState
            subTitle.push(subElment)
            subElment = {}


            element.subtitle = subTitle
            newData.push(element)
         })
         return newData;
      },
      getFormatDate(dt, format2) {
         let newDate = dt
         if (dt.includes("Z[UTC]"))
            newDate = dt.replace("Z[UTC]", "")


         var date = new Date(newDate);
         var dateStr
         if (format2) {

            dateStr =
               ("00" + (date.getMonth() + 1)).slice(-2) + "/" +
               ("00" + date.getDate()).slice(-2) + "/" +
               date.getFullYear() + " " +
               ("00" + date.getHours()).slice(-2) + ":" +
               ("00" + date.getMinutes()).slice(-2) + ":" +
               ("00" + date.getSeconds()).slice(-2);

         } else {
            dateStr =
               ("00" + date.getDate()).slice(-2) + "/" +
               ("00" + (date.getMonth() + 1)).slice(-2) + "/" +
               date.getFullYear() + " " +
               ("00" + date.getHours()).slice(-2) + ":" +
               ("00" + date.getMinutes()).slice(-2) + ":" +
               ("00" + date.getSeconds()).slice(-2);
         }



         return dateStr
      },

      getTime(date) {
         var d = new Date(date);
         var time = moment(d, "YYYY-MM-DD HH:mm:ss").fromNow();

         if (time.includes("year")) time = time.replace("year", "año");
         if (time.includes("months")) time = time.replace("months", "meses");
         if (time.includes("month")) time = time.replace("month", "mes");
         if (time.includes("day")) time = time.replace("day", "dia");
         if (time.includes("hour")) time = time.replace("hour", "hora");
         if (time.includes("minute")) time = time.replace("minute", "minuto");
         if (time.includes("ago")) time = time.replace("ago", "");
         if (time.includes("a ")) time = time.replace("a ", "Un ");
         if (time.includes("in ")) time = time.replace("in ", "En ");
         if (time.includes("an ")) time = time.replace("an ", "una ");
         return time;
      },
      formatItemDetail(user, item) {
         let detail = []
         let element = {}

         element.name = "Usuario"
         element.value = user
         detail.push(element)
         element = {}

         element.name = "Empresa"
         element.value = item.company
         detail.push(element)
         element = {}

         element.name = "Empresa que recibe"
         element.value = item.subtitle[1].value
         detail.push(element)
         element = {}


         if(item.subtitle[4].value){
            element.name = "Motivo rechazo"
            element.value = item.subtitle[4].value
            detail.push(element)
            element = {}
         }

         element.name = "Nombre de tarea"
         element.value = item.title
         detail.push(element)
         element = {}


         element.name = "Fecha entrega"
         element.value = item.subtitle[0].value
         detail.push(element)
         element = {}

         return detail
      },
      formatCurrentDate() {
         var date = new Date()

         var dateStr =
            date.getFullYear() + "-" +
            ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
            ("00" + date.getDate()).slice(-2) + " " +
            ("00" + date.getHours()).slice(-2) + ":" +
            ("00" + date.getMinutes()).slice(-2)

         return dateStr
      },
      validIsNew(id) {
         var flag = false
         var notifications = localStorage.getItem("notifications")
         if (!notifications) return

         notifications.split(",").map(item => {
            if (item == id) {
               flag = true
               return
            }
         })
         return flag

      },
      reverse(s) {
         return s.split("").reverse().join("");
      }
   }
}