
const routes = [
  {
    path: '/',
    component: () => import('components/Security/Validation.vue')
  },
  {
    path: '/Login',
    component: () => import('components/Security/Login.vue')
  },
  {
    path: '/Task',
    props : true,
    component: () => import('components/Task/Home.vue')
  },
  {
    path: '/list',
    component: () => import('components/Common/ListItem.vue')
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
