//export const URL_BASE ="http://192.168.0.124:8080/ccrws/api/"
export const URL_BASE = "http://192.168.1.120:8888/ccrws/api/"
export const Result = {
    SUCCESS : 1,
    EMPTY : 2,
    ERROR : 3,
    CREATE : 4,
    DELETE : 5,
    UPDATE : 6,
    SESSION_STARTED : 7

}